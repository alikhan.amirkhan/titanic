import pandas as pd

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]
    
    # Function to extract title from name
    def extract_title(name):
        for title in titles:
            if title in name:
                return title
        return None

    # Extract titles and add a new column 'Title' to the dataframe
    df['Title'] = df['Name'].apply(extract_title)
    
    # Initialize results list
    results = []

    # Calculate median age for each title group and count missing ages
    for title in titles:
        group = df[df['Title'] == title]
        missing_ages_count = group['Age'].isnull().sum()
        median_age = round(group['Age'].median())
        results.append((title, missing_ages_count, median_age))
        
        # Fill in missing 'Age' values with the median age for the title group
        df.loc[(df['Age'].isnull()) & (df['Title'] == title), 'Age'] = median_age
    
    # Return the results
    return results
